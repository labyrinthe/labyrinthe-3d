#include <stdlib.h>
#include <stdio.h>

#include "laby.h"


static void CasserMur (Laby* lab, size_t j, int mur_a_casser)
{
    lab->etat[j] &= ~mur_a_casser;
    if (mur_a_casser == MUR_G)
    {
        lab->etat[j-1] &= ~MUR_D;
    }
    else if (mur_a_casser == MUR_D)
    {
        lab->etat[j+1] &= ~MUR_G;
    }
    else if (mur_a_casser == MUR_AR)
    {
        lab->etat[j-lab->largeur] &= ~MUR_AV;
    }
    else
    {
        lab->etat[j+lab->largeur] &= ~MUR_AR;
    }
}

static void ParcoursExhaustif(Laby* lab, size_t j)
{
    size_t id_suiv[4], id;
    unsigned nb_possibilites, choix;
    unsigned mur_a_casser[4] = { 0 };
    size_t x, y;

    do
    {
        x = j%lab->largeur;
        y = j/lab->largeur;
        nb_possibilites = 0;

        if (x > 0)
        {
            if ( lab->etat[j-1] == FERME )
            {
                id_suiv[nb_possibilites] = j-1;
                mur_a_casser[nb_possibilites++] = MUR_G;
            }
        }
        if (x < lab->largeur-1)
        {
            if ( lab->etat[j+1] == FERME )
            {
                id_suiv[nb_possibilites] = j+1;
                mur_a_casser[nb_possibilites++] = MUR_D;
            }
        }
        if (y > 0)
        {
            if ( lab->etat[j-lab->largeur] == FERME )
            {
                id_suiv[nb_possibilites] = j-lab->largeur;
                mur_a_casser[nb_possibilites++] = MUR_AR;
            }
        }
        if (y < lab->hauteur-1)
        {
            if ( lab->etat[j+lab->largeur] == FERME )
            {
                id_suiv[nb_possibilites] = j+lab->largeur;
                mur_a_casser[nb_possibilites++] = MUR_AV;
            }
        }

        if (nb_possibilites)
        {
            choix = rand() % nb_possibilites;
            id = id_suiv[choix];
            CasserMur (lab, j, mur_a_casser[choix]);
            lab->id[id] = j;
            j = id;
        }
        else
        {
            j = lab->id[j];
        }
    }
    while (j != 0);
}

static void InitLabyrinthe (Laby* lab)
{
    size_t i;
    for (i=0; i<lab->largeur*lab->hauteur; i++)
    {
        lab->etat[i] = FERME;
        lab->id[i] = 0;
    }
}

void GenereLabyrintheParfait2_2D (Laby* lab)
{
    /* initialise les id, et met toutes les cases a FERME */
    InitLabyrinthe (lab);
    ParcoursExhaustif(lab, 0);
}
