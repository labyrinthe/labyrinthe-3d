#include <stdlib.h>
#include <stdio.h>

#include "laby.h"
#include "aleatoire.h"


/*************************************************
fonction qui parcours les cases reliees a j
(parcours en profondeur) et leur donne l'id voulu
**************************************************/
static void FormateLabyrinthe(Laby* lab, size_t j, size_t id)
{
    char etat = lab->etat[j];
    lab->id[j] = id;
    if ( !(etat&MUR_G) && lab->id[j-1] != id )
    {
        FormateLabyrinthe(lab, j-1, id);
    }
    if ( !(etat&MUR_D) && lab->id[j+1] != id )
    {
        FormateLabyrinthe(lab, j+1, id);
    }
    if ( !(etat&MUR_AR) && lab->id[j-lab->largeur] != id )
    {
        FormateLabyrinthe(lab, j-lab->largeur, id);
    }
    if ( !(etat&MUR_AV) && lab->id[j+lab->largeur] != id )
    {
        FormateLabyrinthe(lab, j+lab->largeur, id);
    }
}

static void CasserMur (Laby* lab, size_t j, unsigned mur_a_casser)
{
    size_t id;
    lab->etat[j] &= ~mur_a_casser;
    if (mur_a_casser == MUR_G)
    {
        id = lab->id[j-1];
        lab->etat[j-1] &= ~MUR_D;
    }
    else if (mur_a_casser == MUR_D)
    {
        id = lab->id[j+1];
        lab->etat[j+1] &= ~MUR_G;
    }
    else if (mur_a_casser == MUR_AR)
    {
        id = lab->id[j-lab->largeur];
        lab->etat[j-lab->largeur] &= ~MUR_AV;
    }
    else
    {
        id = lab->id[j+lab->largeur];
        lab->etat[j+lab->largeur] &= ~MUR_AR;
    }
    /* actualise les id des cellules */
    FormateLabyrinthe(lab, j, id);
}

/**
fonction "principale"
*/
static int FormateCell (Laby* lab, size_t j)
{
    size_t x = j%lab->largeur;
    size_t y = j/lab->largeur;
    size_t curr_id = lab->id[j];
    unsigned nb_possibilites = 0;
    unsigned possibilite[4] = { 0 };
    unsigned mur_a_casser = 0;

    /* si la case n'est pas en bord du labyrinthe */
    if (x > 0)
    {
        /* et si la case voisine n'est pas reliee */
        if ( curr_id != lab->id[j-1] )
        {
            /* on ajoute le mur au tableau des possibilites */
            possibilite[nb_possibilites++] = MUR_G;
        }
    }
    if (x < lab->largeur-1)
    {
        if ( curr_id != lab->id[j+1] )
        {
            possibilite[nb_possibilites++] = MUR_D;
        }
    }
    if (y > 0)
    {
        if ( curr_id != lab->id[j-lab->largeur] )
        {
            possibilite[nb_possibilites++] = MUR_AR;
        }
    }
    if (y < lab->hauteur-1)
    {
        if ( curr_id != lab->id[j+lab->largeur] )
        {
            possibilite[nb_possibilites++] = MUR_AV;
        }
    }
    /* s'il existe un mur a casser */
    if (nb_possibilites > 0)
    {
        /* On choisit aleatoirement le mur a casser */
        mur_a_casser = possibilite[rand() % nb_possibilites];
        CasserMur (lab, j, mur_a_casser);
        /* on retourne que l'on a casse un mur */
        return 1;
    }
    /* sinon on n'a rien casse */
    else return 0;
}

static void InitLabyrinthe (Laby* lab)
{
    size_t i;
    for (i=0; i<lab->largeur*lab->hauteur; i++)
    {
        lab->etat[i] = FERME;
        lab->id[i] = i;
    }
}

void SupprimeLabyrinthe (Laby* lab)
{
    free(lab->etat), lab->etat = NULL;
    free(lab->chemin), lab->chemin = NULL;
    free (lab->id), lab->id = NULL;
    free(lab), lab = NULL;
}

/******************************************
Generation de labyrinthe, suivant l'algo de
fusion aleatoire de chemins
(http://ilay.org/yann/articles/maze/)
*****************************************/
void GenereLabyrintheParfait_2D (Laby* lab)
{
    size_t i = 0;
    /* utilise un generateur de nombre aleatoires uniques
       pour une generation plus rapide */
    if ( InitGenerateur (0, (lab->largeur*lab->hauteur)-1))    {
        /* initialise les id, et met toutes les cases a FERME */
        InitLabyrinthe (lab);
        /* tant qu'il reste des murs a casser */
        while ( i < (lab->largeur*lab->hauteur)-1 )
        {
            i += FormateCell( lab, (unsigned) ReturnAleatoire ());
        }
        TermineGenerateur();
    }
}
Laby* CreeLabyrinthe (size_t x, size_t y)
{
    Laby* lab = malloc (sizeof *lab);
    lab->largeur = x, lab->hauteur = y;
    lab->etat = malloc (sizeof *(lab->etat) * x * y);
    lab->id = malloc (sizeof *(lab->id) * x * y);
    lab->chemin = malloc (sizeof *(lab->chemin) * x * y);
    return lab;
}
