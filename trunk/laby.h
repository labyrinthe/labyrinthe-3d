#ifndef LABY_H
#define LABY_H

#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>

#define MUR_G   0x1
#define MUR_D   0x2
#define MUR_AR  0x4
#define MUR_AV  0x8
#define FERME (MUR_G|MUR_D|MUR_AR|MUR_AV)

/* Structure contenant les parametres du labyrinthe */
typedef struct
{
    size_t largeur;
    size_t hauteur;
    size_t arrivee;
    char* etat;     /* etat de la cellule (murs) */
    char* chemin;   /* BOOL ; si chemin : 1, sinon : 0*/
    size_t* id;     /* enregistre si la cellule est reliee au labyrinthe */
} Laby;

/* Structure pour effectuer la recherche du plus court chemin */
typedef struct
{
    size_t* prec_id;    /* id de la case precedente la plus proche */
    size_t* file;       /* file pour parcours en largeur */
    char* visited;      /* BOOL ; si visite : 1, sinon : 0 */
}
T_Search;


Laby* CreeLabyrinthe (size_t x, size_t y);
void GenereLabyrintheParfait_2D (Laby* lab) ;
void GenereLabyrintheParfait2_2D (Laby* lab) ;
void SupprimeLabyrinthe (Laby* lab);

int RechercheChemin (Laby* lab, size_t depart, size_t arrivee);
void InitChemin (Laby* lab);


#endif
