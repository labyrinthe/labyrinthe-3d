#include "laby.h"
#include "affichage.h"

GLuint texture[3];


void DrawLabyrinthe (Laby* lab)
{
    Coordonnees_3D v1,v2,v3,v4,v5,v6,v7,v8;
    unsigned x,z,id;

    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

#define LUM_DIFF    1.0f
#define LUM_AMB     0.5f
    GLfloat LightAmbient[]= { LUM_AMB, LUM_AMB, LUM_AMB, 1.0f };
    GLfloat LightDiffuse[]= { LUM_DIFF, LUM_DIFF, LUM_DIFF, 1.0f };
    //GLfloat LightPosition[]= { (double)lab->largeur/2, 2, -(double)lab->hauteur/2, 1.0f };
    GLfloat LightPosition[]= { (double)lab->largeur*0.5, 3, -(double)lab->hauteur*0.5, 1.0f };
    glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);
    glEnable(GL_LIGHT0);

    /* sol */
    //glColor3ub(255,255,255); //blanc
    glBindTexture(GL_TEXTURE_2D, texture[0]);
    glBegin(GL_QUADS);
    glTexCoord2d(0,0);
    glVertex3d(0,0,0);
    glTexCoord2d(0,lab->hauteur);
    glVertex3d(0,0,-(double)lab->hauteur);
    glTexCoord2d(lab->largeur,lab->hauteur);
    glVertex3d(lab->largeur,0,-(double)lab->hauteur);
    glTexCoord2d(lab->largeur,0);
    glVertex3d(lab->largeur,0,0);
    glEnd();

    /* murs */
    glBindTexture(GL_TEXTURE_2D, texture[1]);
    glBegin(GL_QUADS);
    for (z=0; z<lab->hauteur; z++)
        for (x=0; x<lab->largeur; x++)
        {
            id = x+z*lab->largeur;
            v1.x = x, v1.y = 0, v1.z = -(double)z;
            v2.x = v1.x, v2.y = 0, v2.z = v1.z-1;
            v3.x = v2.x+1, v3.y = 0, v3.z = v2.z;
            v4.x = v3.x, v4.y = 0, v4.z = v1.z;
            v5.x = v1.x, v5.y = 1, v5.z = v1.z;
            v6.x = v2.x, v6.y = 1, v6.z = v2.z;
            v7.x = v3.x, v7.y = 1, v7.z = v3.z;
            v8.x = v4.x, v8.y = 1, v8.z = v4.z;

            if (x == 0 && lab->etat[id]&MUR_G)
            {
                //glColor3ub(255,0,0); //rouge
                glTexCoord2d(0,3);
                glVertex3d(v5.x,v5.y,v5.z);
                glTexCoord2d(3,3);
                glVertex3d(v6.x,v6.y,v6.z);
                glTexCoord2d(3,0);
                glVertex3d(v2.x,v2.y,v2.z);
                glTexCoord2d(0,0);
                glVertex3d(v1.x,v1.y,v1.z);
            }

            if (lab->etat[id]&MUR_D)
            {
                //glColor3ub(0,255,0); //vert
                glTexCoord2d(3,3);
                glVertex3d(v8.x,v8.y,v8.z);
                glTexCoord2d(0,3);
                glVertex3d(v7.x,v7.y,v7.z);
                glTexCoord2d(0,0);
                glVertex3d(v3.x,v3.y,v3.z);
                glTexCoord2d(3,0);
                glVertex3d(v4.x,v4.y,v4.z);
            }

            if (lab->etat[id]&MUR_AV)
            {
                //glColor3ub(0,0,255); //bleu
                glTexCoord2d(0,3);
                glVertex3d(v6.x,v6.y,v6.z);
                glTexCoord2d(3,3);
                glVertex3d(v7.x,v7.y,v7.z);
                glTexCoord2d(3,0);
                glVertex3d(v3.x,v3.y,v3.z);
                glTexCoord2d(0,0);
                glVertex3d(v2.x,v2.y,v2.z);
            }

            if (z == 0 && lab->etat[id]&MUR_AR)
            {
                //glColor3ub(255,255,0); //jaune
                glTexCoord2d(3,3);
                glVertex3d(v5.x,v5.y,v5.z);
                glTexCoord2d(0,3);
                glVertex3d(v8.x,v8.y,v8.z);
                glTexCoord2d(0,0);
                glVertex3d(v4.x,v4.y,v4.z);
                glTexCoord2d(3,0);
                glVertex3d(v1.x,v1.y,v1.z);
            }
            //glColor3ub(255,255,255);
        }
    glEnd();

    /* sortie (derniere case) */
    glBindTexture(GL_TEXTURE_2D, texture[2]);
    glBegin(GL_QUADS);
    glTexCoord2d(0,1);
    glVertex3d(v6.x,v6.y,v6.z);
    glTexCoord2d(1,1);
    glVertex3d(v7.x,v7.y,v7.z);
    glTexCoord2d(1,0);
    glVertex3d(v3.x,v3.y,v3.z);
    glTexCoord2d(0,0);
    glVertex3d(v2.x,v2.y,v2.z);
    glEnd();

    glFlush();
}

void DirigeCamera (Camera* camera)
{
    Coordonnees_3D cible;
    cible.x = camera->cam.x + cos(camera->angleXZ);
    cible.y = camera->cam.y;
    cible.z = camera->cam.z + sin(camera->angleXZ);
    gluLookAt(camera->cam.x,camera->cam.y,camera->cam.z, cible.x,cible.y,cible.z, 0,1,0);
}

/* gestion des collisions basique */
void AvancerCamera (Laby* lab, Camera* camera, int direction)
{
    unsigned collision = 1, id;
    Coordonnees_3D tmp;
    tmp.x = camera->cam.x + (0.3 * cos(camera->angleXZ) * direction);
    tmp.z = camera->cam.z + (0.3 * sin(camera->angleXZ) * direction);
    if (tmp.x > 0 && tmp.z < 0)
    {
        if ( (int) tmp.x == (int) camera->cam.x && (int) tmp.z == (int) camera->cam.z)
        {
            collision = 0;
        }
        else
        {
            id = (int) camera->cam.x - (int) camera->cam.z*lab->largeur;
            if ((int) tmp.x != (int) camera->cam.x)
            {
                if (tmp.x > camera->cam.x)
                {
                    if (!(lab->etat[id]&MUR_D))
                        collision = 0;
                }
                else
                {
                    if (!(lab->etat[id]&MUR_G))
                        collision = 0;
                }
            }
            if ((int) tmp.z != (int) camera->cam.z)
            {
                if (tmp.z < camera->cam.z)
                {
                    if (!(lab->etat[id]&MUR_AV))
                        collision = 0;
                }
                else
                {
                    if (!(lab->etat[id]&MUR_AR))
                        collision = 0;
                }
            }
        }
    }
    if (!collision)
    {
        camera->cam.x += (0.166 * cos(camera->angleXZ) * direction);
        camera->cam.z += (0.166 * sin(camera->angleXZ) * direction);
    }
}

void AffichageLabyrinthe (Laby* lab, Camera* camera)
{
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
    DirigeCamera (camera);
    DrawLabyrinthe (lab);
    SDL_GL_SwapBuffers();
}

static void NormalizeAngle (GLdouble* angle)
{
    while (*angle < -M_PI)
        *angle += 2*M_PI;
    while (*angle > M_PI)
        *angle -= 2*M_PI;
}

/* Bugs :
 - ne fonctionne pas sur toutes les positions.
*/
void PiloteAutomatique (Laby* lab, Camera* camera)
{
    Camera cameraAuto;
    cameraAuto.cam.x = floor(camera->cam.x)+0.5;
    cameraAuto.cam.y = camera->cam.y;
    cameraAuto.cam.z = floor(camera->cam.z)+0.5;
    cameraAuto.angleXZ = camera->angleXZ;
    size_t id, n;
    while ( (id = (int)cameraAuto.cam.x - ((int)cameraAuto.cam.z*lab->largeur)) != lab->arrivee)
    {
        lab->chemin[id] = 0;
        NormalizeAngle (&cameraAuto.angleXZ);
        printf("id : %u\n", id);
        if (!(lab->etat[id]&MUR_D) && lab->chemin[id+1])
        {
            puts("a droite");
            while (cameraAuto.angleXZ > 0.16 || cameraAuto.angleXZ < -0.16)
            {
                if (cameraAuto.angleXZ > 0)
                    cameraAuto.angleXZ -= 0.14;
                else
                    cameraAuto.angleXZ += 0.14;
                AffichageLabyrinthe (lab, &cameraAuto);
                SDL_Delay(70);
            }
            cameraAuto.angleXZ = 0;
        }
        else if (!(lab->etat[id]&MUR_G) && lab->chemin[id-1])
        {
            puts("a gauche");
            while (cameraAuto.angleXZ > -M_PI + 0.16 && cameraAuto.angleXZ < M_PI - 0.16)
            {
                if (cameraAuto.angleXZ < 0)
                    cameraAuto.angleXZ -= 0.14;
                else
                    cameraAuto.angleXZ += 0.14;
                AffichageLabyrinthe (lab, &cameraAuto);
                SDL_Delay(70);
            }
            cameraAuto.angleXZ = -M_PI;
        }
        else if (!(lab->etat[id]&MUR_AV) && lab->chemin[id+lab->largeur])
        {
            puts("avant");
            while ( (cameraAuto.angleXZ < -M_PI/2 - 0.16 || cameraAuto.angleXZ > -M_PI/2 + 0.16) && cameraAuto.angleXZ < (M_PI*1.5) - 0.16)
            {
                if (cameraAuto.angleXZ > M_PI/2 || cameraAuto.angleXZ < -M_PI/2)
                    cameraAuto.angleXZ += 0.14;
                else
                    cameraAuto.angleXZ -= 0.14;
                AffichageLabyrinthe (lab, &cameraAuto);
                SDL_Delay(70);
            }
            cameraAuto.angleXZ = -M_PI/2;
        }
        else if (!(lab->etat[id]&MUR_AR) && lab->chemin[id-lab->largeur])
        {
            puts("arriere");
            while (cameraAuto.angleXZ > -(M_PI*1.5) + 0.16 && (cameraAuto.angleXZ < (M_PI/2) - 0.16 || cameraAuto.angleXZ > (M_PI/2) + 0.16))
            {
                if (cameraAuto.angleXZ < -M_PI/2 || cameraAuto.angleXZ > M_PI/2)
                    cameraAuto.angleXZ -= 0.14;
                else
                    cameraAuto.angleXZ += 0.14;
                AffichageLabyrinthe (lab, &cameraAuto);
                SDL_Delay(70);
            }
            cameraAuto.angleXZ = M_PI/2;
        }

        for (n=0; n<6; n++)
        {
            AvancerCamera (lab, &cameraAuto, AVANT);
            AffichageLabyrinthe (lab, &cameraAuto);
            SDL_Delay(70);
        }
    }
    NormalizeAngle (&cameraAuto.angleXZ);
    while ( (cameraAuto.angleXZ < -M_PI/2 - 0.16 || cameraAuto.angleXZ > -M_PI/2 + 0.16) && cameraAuto.angleXZ < (M_PI*1.5) - 0.16)
    {
        if (cameraAuto.angleXZ > M_PI/2 || cameraAuto.angleXZ < -M_PI/2)
            cameraAuto.angleXZ += 0.14;
        else
            cameraAuto.angleXZ -= 0.14;
        AffichageLabyrinthe (lab, &cameraAuto);
        SDL_Delay(70);
    }
    SDL_Delay(1000);
}
