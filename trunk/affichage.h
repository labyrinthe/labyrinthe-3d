#ifndef AFFICHAGE_H
#define AFFICHAGE_H

#include <math.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>
#include <SDL/SDL.h>

#include "laby.h"

#define SCR_LARGEUR 800
#define SCR_HAUTEUR 600
#define FOVY        70
#define RATIO       ((double)SCR_LARGEUR/SCR_HAUTEUR)
#define NEAR        0.01
#define FAR         10000

#define AVANT       1
#define ARRIERE     -1

typedef struct
{
    GLdouble x;
    GLdouble y;
    GLdouble z;
} Coordonnees_3D;

typedef struct
{
    Coordonnees_3D cam;
    GLdouble angleXZ;
} Camera;


void DrawLabyrinthe (Laby* lab);
void DirigeCamera (Camera* camera);
void AvancerCamera (Laby* lab, Camera* camera, int direction);
void AffichageLabyrinthe (Laby* lab, Camera* camera);
GLuint loadTexture (const char* filename);
void PiloteAutomatique (Laby* lab, Camera* camera);

#endif
