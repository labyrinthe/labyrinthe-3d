#include <time.h>
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "laby.h"
#include "affichage.h"

extern GLuint texture[3];


#define SCR_LARGEUR 800
#define SCR_HAUTEUR 600
#define CASES_X     10
#define CASES_Y     10

int main ( int argc, char** argv )
{
    int continuer = 0, quitter = 0, draw = 1;
    Laby* lab = NULL;
    Camera camera;
    SDL_Event event;

    srand(time(NULL));
    /*  initialize SDL video */
    SDL_Init(SDL_INIT_VIDEO);
    /*  create a new window */
    SDL_SetVideoMode(SCR_LARGEUR, SCR_HAUTEUR, 32, SDL_OPENGL/*|SDL_FULLSCREEN*/);
    SDL_EnableKeyRepeat(70, 70);
    SDL_ShowCursor(SDL_DISABLE);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(FOVY, RATIO, NEAR, FAR);

    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);

    texture[0] = loadTexture("sol1.bmp");
    texture[1] = loadTexture("brique.bmp");
    texture[2] = loadTexture("sortie.bmp");

    if ( (lab = CreeLabyrinthe (CASES_X, CASES_Y)) != NULL)
    {
        while (!quitter)
        {
            /* Initialise le labyrinthe et les positions */
            GenereLabyrintheParfait_2D (lab);
            /* definit l'arrivee : il faudrait mette un truc special au depart et a l'arrivee */
            lab->arrivee = (CASES_X*CASES_Y)-1;
            lab->etat[lab->arrivee] &= ~MUR_AV;
            camera.cam.x = 0.5, camera.cam.y = 0.7, camera.cam.z = -0.5;
            camera.angleXZ =  -M_PI/2;
            continuer = 1;
            /* Boucle evenementielle */
            do
            {
                if (draw)
                {
                    AffichageLabyrinthe (lab, &camera);
                    draw = 0;
                }
                SDL_WaitEvent(&event);
                switch (event.type)
                {
                case SDL_QUIT:
                    quitter = 1;
                    break;
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym)
                    {
                    case SDLK_PAGEUP:
                        if (camera.cam.y < 7)
                        {
                            camera.cam.y += 0.2;
                            draw = 1;
                        }
                        break;
                    case SDLK_PAGEDOWN:
                        if (camera.cam.y > 0.2)
                        {
                            camera.cam.y -= 0.2;
                            draw = 1;
                        }
                        break;
                    case SDLK_UP:    // Fl�che haut
                        AvancerCamera (lab, &camera, AVANT);
                        draw = 1;
                        break;
                    case SDLK_DOWN:    // Fl�che haut
                        AvancerCamera (lab, &camera, ARRIERE);
                        draw = 1;
                        break;
                    case SDLK_RIGHT: // Fl�che droite
                        camera.angleXZ += 0.1;
                        draw = 1;
                        break;
                    case SDLK_LEFT:  // Fl�che gauche
                        camera.angleXZ -= 0.1;
                        draw = 1;
                        break;
                    case SDLK_F1:    // Aide
                        RechercheChemin (lab, (int)camera.cam.x - ((int)camera.cam.z*lab->largeur), lab->arrivee);
                        PiloteAutomatique (lab, &camera);
                        draw = 1;
                        break;
                    case SDLK_F2:    // Refresh
                        continuer = 0;
                        draw = 1;
                        break;
                    case SDLK_ESCAPE:
                        quitter = 1;
                        break;
                    default:
                        break;
                    }
                    break;
                }
            }
            while (continuer && !quitter );
        }

        SupprimeLabyrinthe (lab);
    }
    SDL_Quit();
    return EXIT_SUCCESS;
}
